// to check connection b/w js and html
// console.log("connected")


// geting all inputs from user
const numberOne = document.getElementById("subject-one")
const numberTwo = document.getElementById("subject-two")
const numberTree = document.getElementById("subject-three")
const numberFour = document.getElementById("subject-four")
const numberFive = document.getElementById("subject-five")

const btnSubmit = document.getElementById("btn-submit")


//  creating a function to calculate score
function calcmarks() {
    let n1 = numberOne.value;
    let n2 = numberTwo.value;
    let n3 = numberTree.value;
    let n4 = numberFour.value;
    let n5 = numberFive.value;

    const marksResult = parseInt(n1) + parseInt(n2) + parseInt(n3) + parseInt(n4) + parseInt(n5)

// using if and else if statement 
    if (marksResult >= 450) {
        alert("You scored grade A - Congratulations")

    } else if (marksResult > 300 && marksResult < 450) {
        alert("You scored grade B - Good Job")

    } else if (marksResult > 250 && marksResult < 300) {
        alert("You scored grade C - You can do better")

    } else {
        alert("Need to Work Hard")
    }

}


// adding function to button
btnSubmit.addEventListener("click", calcmarks);





